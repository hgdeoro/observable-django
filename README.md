# Lightweight observability stack for Django app

I'm creating this repository with the purpose to lear now to set up observability on a Django project.

Stack should be lightweight (avoid JVM) and even better if data and UI is cloud based.

In the context of this project, observability means:
 - Logs
 - Metrics
 - Traces

At the moment I use Grafana Cloud. It has a good free tier and paid plans are cheap and powerful.

```mermaid
sequenceDiagram
    python-cli->>nginx: HTTP GET
    nginx->>uvicorn-django: HTTP GET
    uvicorn-django->>postgresql: sql query
    uvicorn-django->>redis: redis query
    uvicorn-django->>minio: boto queries
    python-cli-->>collector: spans
    nginx-->>collector: spans
    uvicorn-django-->>collector: spans
    collector-->>grafana-cloud: spans
    fluentbit-->>grafana-cloud: logs
    fluentbit-->>grafana-cloud: metrics
```

Some more info is available at [wiki](https://gitlab.com/hgdeoro/observable-django/-/wikis/Home)

## Traces

![Screenshot of trace](trace-detail.jpg "Screenshot of trace")

Will use OpenTelemetry only for traces at the moment (https://opentelemetry.io/docs/instrumentation/python/):

    The current status of the major functional components for OpenTelemetry Python is as follows:
    - Traces (API, SDK): Stable
    - Metrics (API, SDK): RC
    - Logs (SDK): Experimental

Initial setup based on example from: https://github.com/open-telemetry/opentelemetry-python/tree/main/docs/examples

To use it, you need to configure some parameters in environment file:

    $ cp otel-collector/grafana-cloud-env.template otel-collector/grafana-cloud-env
    $ vi otel-collector/grafana-cloud-env

Should look something like:

    GRAFANA_CLOUD_TEMPO_AUTHORIZATION='base64-of-concatenation-of-user-colon-api-key'


Resources:
 * https://opentelemetry.io/docs/instrumentation/python/
 * https://opentelemetry-python.readthedocs.io/en/latest/
 * https://grafana.com/blog/2021/04/13/how-to-send-traces-to-grafana-clouds-tempo-service-with-opentelemetry-collector/

To ship traces to Grafana Cloud you need to know your endpoint, username and create an API KEY:
from Grafana Cloud 'home', go to 'Stacks', look for 'Tempo' and clic 'Details', you'll find endpoint and username,
and from there you'll be able to create an API KEY (as of 2022-09-27 this works).

If authentication is not configured in the right way, you might see this in Open Collector's logs:

    Permanent error: rpc error: code = Unauthenticated desc = unexpected HTTP status code received from server: 401 (Unauthorized);

### What's instrumented

* queries from the cli client `python-app-src/client.py`
* request from nginx to gunicorn/uvicorn/django
* request to PostgreSql
* request to Redis
* request to Minio
* http requests created using `requests` library
* explicitly create span to trace pieces of code
* asgi

Explicitly create span to trace pieces of code:

    def _do_some_work_that_takes_some_time():
        tracer = trace.get_tracer(__name__)
        with tracer.start_as_current_span("some_work_that_takes_some_time"):
            time.sleep(0.77)


### What's NOT instrumented

* Django templates (instrumentation for Jinja2 is available)

## Logs

![Screenshot of logs](logs-detail.jpg "Screenshot of logs")

Shipping logs to Grafana Cloud is not easy to configure.

The steps required to get the URL, user and password is documented here:
https://grafana.com/docs/grafana-cloud/quickstart/logs_promtail_linuxnode/#configure-grafana-cloud-with-a-loki-data-source

To use it, you need to configure some parameters in environment file:

    $ cp fluent-bit/grafana-cloud-env.template fluent-bit/grafana-cloud-env
    $ vi fluent-bit/grafana-cloud-env

Should look something like:

    GRAFANA_CLOUD_LOGS_HOST=logs-xxxxxxxx.grafana.net
    GRAFANA_CLOUD_LOGS_HTTP_USER=123456789
    GRAFANA_CLOUD_LOGS_HTTP_PASSWD='some-long-text-that-looks-like-base64'

TODO:
 * tag logs with name of container or in some way so they are easy to identify

## Metrics

Metrics are collected by **fluentbit** and send to local collector.

Seems like both **fluentbit** (https://calyptia.com/2021/08/11/use-fluent-bit-to-send-logs-and-metrics-to-grafana-cloud/)
and **opentelemetry-collector** (https://opentelemetry.io/docs/reference/specification/metrics/) can also ship metrics.

# Websockets

To send a broadcast message to clients, run:

   $ cd python-app-src
   $ OTEL_PYTHON_DJANGO_INSTRUMENT=False python3 manage.py broadcast_message "Hello world :)"

# Next steps

 * integrate **traces** with **logging**: https://opentelemetry-python-contrib.readthedocs.io/en/latest/instrumentation/logging/logging.html
 * add exception information to traces
 * try stuff from https://opentelemetry.io/docs/instrumentation/python/manual/
    * decorator to create spans
    * add events
    * play with errors
 * Try Grafana Agent for collecting logs/metrics/traces
    * https://community.grafana.com/t/add-container-name-to-promtail-docker-logs/58572
    * https://grafana.com/docs/loki/latest/clients/promtail/configuration/#docker_sd_config

# How to start everything

    $ make compose-pull compose-build
    $ make compose-up
