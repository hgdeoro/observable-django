import json

from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer

ROOM_GROUP_NAME = "default-group"


class SampleConsumer(WebsocketConsumer):
    def connect(self):
        async_to_sync(self.channel_layer.group_add)(ROOM_GROUP_NAME, self.channel_name)
        self.accept()

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(ROOM_GROUP_NAME, self.channel_name)

    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json["message"]

        # Send message to room group
        group_send = async_to_sync(self.channel_layer.group_send)
        group_send(ROOM_GROUP_NAME, {"type": "app_message", "message": message})

    def app_message(self, event):
        message = event["message"]

        # Send message to WebSocket
        self.send(text_data=json.dumps({"message": message}))
