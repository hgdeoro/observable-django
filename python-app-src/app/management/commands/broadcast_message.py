from app.websockets.consumers import ROOM_GROUP_NAME
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = "Broadcast a message to user using websockets"

    def add_arguments(self, parser):
        parser.add_argument("message")

    def handle(self, *args, **options):
        message = options["message"]

        channel_layer = get_channel_layer()
        group_send = async_to_sync(channel_layer.group_send)

        group_send(
            ROOM_GROUP_NAME,
            {
                "type": "app_message",
                "message": message,
            },
        )

        self.stdout.write(self.style.SUCCESS("Successfully sent"))
