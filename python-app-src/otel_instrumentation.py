import os

OTEL_STDOUT = False
OTEL_OTLP = True
OTEL_DJANGO = True
OTEL_PSYCOPG = True
OTEL_REDIS = True
OTEL_BOTO = True
OTEL_REQUESTS = True


def instrument_all():
    if os.environ.get("OTEL_PYTHON_DJANGO_INSTRUMENT") == "False":
        print("Won't setup OpenTelemetry (OTEL_PYTHON_DJANGO_INSTRUMENT=False)")
        return

    if OTEL_STDOUT:
        print("Setting up OpenTelemetry >> STDOUT")
        from opentelemetry import trace
        from opentelemetry.sdk.trace import TracerProvider
        from opentelemetry.sdk.trace.export import (
            BatchSpanProcessor,
            ConsoleSpanExporter,
        )

        trace.set_tracer_provider(TracerProvider())
        trace.get_tracer_provider().add_span_processor(BatchSpanProcessor(ConsoleSpanExporter()))

    if OTEL_OTLP:
        print("Setting up OpenTelemetry >> OpenCollector")
        from opentelemetry import trace
        from opentelemetry.exporter.otlp.proto.grpc.trace_exporter import (
            OTLPSpanExporter,
        )
        from opentelemetry.sdk.resources import SERVICE_NAME, Resource
        from opentelemetry.sdk.trace import TracerProvider
        from opentelemetry.sdk.trace.export import BatchSpanProcessor

        OTLP_SERVER = os.environ.get("OTLP_SERVER", "127.0.0.1")

        # Service name is required for most backends
        resource = Resource(attributes={SERVICE_NAME: "django-app"})

        provider = TracerProvider(resource=resource)
        processor = BatchSpanProcessor(
            OTLPSpanExporter(
                endpoint=f"{OTLP_SERVER}:4317",
                insecure=True,
            )
        )
        provider.add_span_processor(processor)
        trace.set_tracer_provider(provider)

    if OTEL_DJANGO:
        print("Setting up OpenTelemetry >> DjangoInstrumentor")
        from opentelemetry.instrumentation.django import DjangoInstrumentor

        DjangoInstrumentor().instrument(is_sql_commentor_enabled=True)

    if OTEL_PSYCOPG:
        print("Setting up OpenTelemetry >> Psycopg2Instrumentor")
        from opentelemetry.instrumentation.psycopg2 import Psycopg2Instrumentor

        Psycopg2Instrumentor().instrument(enable_commenter=True, commenter_options={})

    if OTEL_REDIS:
        print("Setting up OpenTelemetry >> RedisInstrumentor")
        from opentelemetry.instrumentation.redis import RedisInstrumentor

        RedisInstrumentor().instrument()

    if OTEL_BOTO:
        print("Setting up OpenTelemetry >> BotocoreInstrumentor")
        from opentelemetry.instrumentation.botocore import BotocoreInstrumentor

        BotocoreInstrumentor().instrument()

    if OTEL_REQUESTS:
        print("Setting up OpenTelemetry >> RequestsInstrumentor")
        from opentelemetry.instrumentation.requests import RequestsInstrumentor

        RequestsInstrumentor().instrument()
