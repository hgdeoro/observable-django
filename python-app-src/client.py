# Copyright The OpenTelemetry Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os

import requests
from opentelemetry import trace
from opentelemetry.propagate import inject


def setup_otel_stdout():
    from opentelemetry.sdk.trace import TracerProvider
    from opentelemetry.sdk.trace.export import BatchSpanProcessor, ConsoleSpanExporter

    trace.set_tracer_provider(TracerProvider())
    trace.get_tracer_provider().add_span_processor(BatchSpanProcessor(ConsoleSpanExporter()))


def setup_otel_otlp():
    from opentelemetry.exporter.otlp.proto.grpc.trace_exporter import OTLPSpanExporter
    from opentelemetry.sdk.resources import SERVICE_NAME, Resource
    from opentelemetry.sdk.trace import TracerProvider
    from opentelemetry.sdk.trace.export import BatchSpanProcessor, ConsoleSpanExporter

    # Service name is required for most backends
    resource = Resource(attributes={SERVICE_NAME: "client"})

    provider = TracerProvider(resource=resource)
    processor = BatchSpanProcessor(
        OTLPSpanExporter(
            endpoint="127.0.0.1:4317",
            insecure=True,
        )
    )
    provider.add_span_processor(BatchSpanProcessor(ConsoleSpanExporter()))
    provider.add_span_processor(processor)
    trace.set_tracer_provider(provider)


def _url() -> str:
    url = os.environ.get("URL", "/")
    if url.startswith("/"):
        return url
    else:
        return f"/{url}"


def main():
    setup_otel_otlp()
    tracer = trace.get_tracer_provider().get_tracer(__name__)

    with tracer.start_as_current_span("client"):
        with tracer.start_as_current_span("client-server"):
            headers = {}
            inject(headers)
            requested = requests.get(
                f"http://localhost:{int(os.environ.get('PORT', '8081'))}{_url()}",
                params={},
                headers=headers,
            )

            assert requested.status_code == 200


if __name__ == "__main__":
    main()
