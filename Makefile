VENVDIR ?= $(abspath ./venv)

export PATH := $(VENVDIR)/bin:$(PATH)

export DOCKER_SOCKET_GROUP ?= $(shell stat -c '%g' /var/run/docker.sock)

export DJANGO_SETTINGS_MODULE ?= proj.settings

pip-compile:
	$(VENVDIR)/bin/pip-compile -o python-app-src/requirements.txt python-app-src/requirements.in
	$(VENVDIR)/bin/pip-compile -o requirements.txt python-app-src/requirements.in requirements.in

pip-compile-upgrade:
	$(VENVDIR)/bin/pip-compile --upgrade -o python-app-src/requirements.txt python-app-src/requirements.in
	$(VENVDIR)/bin/pip-compile --upgrade -o requirements.txt python-app-src/requirements.in requirements.in

pip-sync:
	$(VENVDIR)/bin/pip-sync requirements.txt

lint:
	$(VENVDIR)/bin/black --line-length 120 python-app-src
	$(VENVDIR)/bin/isort --profile black python-app-src
	$(VENVDIR)/bin/flake8 --max-line-length 120 python-app-src

runserver:
	cd python-app-src && \
		env OTEL_PYTHON_DJANGO_INSTRUMENT=False \
			python3 manage.py runserver 0.0.0.0:8081

runserver-otel:
	cd python-app-src && \
		python3 manage.py runserver --noreload 0.0.0.0:8081

daphne:
	cd python-app-src && \
		env OTEL_PYTHON_DJANGO_INSTRUMENT=False PYTHONPATH=$(pwd):$(PATH) \
			daphne -b 0.0.0.0 -p 8081 proj.asgi:application

daphne-otel:
	cd python-app-src && \
		env PYTHONPATH=$(pwd):$(PATH) \
			daphne -b 0.0.0.0 -p 8081 proj.asgi:application

gunicorn:
	cd python-app-src && \
		env OTEL_PYTHON_DJANGO_INSTRUMENT=False PYTHONPATH=$(pwd):$(PATH) \
			gunicorn -b 0.0.0.0:8081 proj.asgi:application -k uvicorn.workers.UvicornWorker

gunicorn-otel:
	cd python-app-src && \
		env PYTHONPATH=$(pwd):$(PATH) \
			gunicorn -b 0.0.0.0:8081 proj.asgi:application -k uvicorn.workers.UvicornWorker

client-otel:
	opentelemetry-instrument --disabled_instrumentations django \
		python3 python-app-src/client.py

# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
# docker compose
# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

compose-logs:
	$(VENVDIR)/bin/docker-compose logs -f --tail=10

compose-build:
	$(VENVDIR)/bin/docker-compose build

compose-pull:
	$(VENVDIR)/bin/docker-compose pull

compose-up:
	$(VENVDIR)/bin/docker-compose up $(UP_ARGS)

compose-up-db:
	$(VENVDIR)/bin/docker-compose up $(UP_ARGS) db

compose-up-redis:
	$(VENVDIR)/bin/docker-compose up $(UP_ARGS) redis

compose-up-minio:
	$(VENVDIR)/bin/docker-compose up $(UP_ARGS) minio

compose-up-django:
	$(VENVDIR)/bin/docker-compose up $(UP_ARGS) django

compose-up-fluent-bit:
	$(VENVDIR)/bin/docker-compose up $(UP_ARGS) fluent-bit

compose-up-otel-collector:
	$(VENVDIR)/bin/docker-compose up $(UP_ARGS) otel-collector

compose-up-nginx:
	$(VENVDIR)/bin/docker-compose up $(UP_ARGS) nginx

compose-up-dependencies:
	$(VENVDIR)/bin/docker-compose up $(UP_ARGS) db redis minio otel-collector
